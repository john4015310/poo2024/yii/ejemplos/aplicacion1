<?php

use yii\helpers\Html;
?>
<h1>Imagen</h1>

<div>
    <?= Html::img("@web/imgs/{$foto}", 
    [//ruta de la imagen
        'alt' => 'Yii Framework',
        'class' => 'img-responsive rounded col-sm-6 img-thumbnail ']) ?>
</div>