<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //esta es la accion que se ejecuta en el primer controlador
        return $this->render('index');//renderiza la vista index
    }

    public function actionPrueba(){
        return $this->render('prueba',[
            'valor' => 'aqui estoy'
        ]);
    }

    public function actionMensaje(){
        return $this->render('mostrarMensaje');
    }

    public function actionImagen($id=0){
        //estt lo leria desde un base de datos
        $imagenes = [
            'pic.jpg',
            'foto1.jpg',
            'foto2.jpg',
            'foto3.jpg'
        ];
        
        return $this->render('imagen',[
            'foto' => $imagenes[$id]
        ]);
    }

    public function actionDias(){
        $dias=[
            'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'
        ];
        return $this->render('diasSemana',[
            'dias' => $dias
        ]);
        
    }

    public function actionImagenes(){
        return $this->render('imagenes');
    }

    public function actionMeses(){
        $datos =[
            'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ];

        return $this->render('meses',[
            'meses' => $datos
        ]);
    }

    public function actionAccion1(){
        //recoge los datos del modelo o de un formulario
        // y los trata
        $numero=1;
        $numero1=2;
        $suma=$numero+$numero1;
        $datos=[
            1,3,5,7,9
        ];
        $mayor=max($datos);

        $texto = "Ejemplo de clase";
        $e= substr_count(strtolower($texto),'e');
        return $this->render('accion1',[
            'numeros' => [$numero, $numero1],
            'suma' => $suma,
            'datos' => $datos,
            'mayor' => $mayor,
            'texto' => $texto,
            'ne' => $e
        ]);
    }
    
    
}
